const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: true}))

mongoose.connect("mongodb+srv://admin123:admin123@project0.cutbola.mongodb.net/contactForm", { useNewUrlParser: true}, { useUnifiedTopology: true})

//create a data schema
const contactSchema = {
	nameCompany: String,
	email: String,
	contactNumber: Number,
	jobPost: String,
	message: String
}

const Contact = mongoose.model("Contact", contactSchema)

app.get("/", function(req, res) {
	res.sendFile(__dirname + "/index.html");
})

app.post("/", function(req,res) {
	let newContact = new Contact({
	nameCompany: req.body.nameCompany,
	email: req.body.email,
	contactNumber: req.body.contactNumber,
	jobPost: req.body.jobPost,
	message: req.body.message
	})
	
	newContact.save();
	res.redirect('/')

})


app.listen(3000, function() {
	console.log("server is running on 3000");
})